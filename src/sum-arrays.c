#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct ArraySum_s
{
    int nel;
    int in1[ArrayLength]; int in2[ArrayLength];  // int *in1; int *in2;
    int out[ArrayLength];
    void (*CalcSum)(struct ArraySum_s *);
    void (*Destroy)(struct ArraySum_s *);
} ArraySum_s;

static void Sum_implementation(ArraySum_s *sum)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<sum->nel; i++) { sum->out[i] = sum->in1[i] + sum->in2[i]; }
}

static void Destroy_implementation(ArraySum_s *sum) { free(sum); }

ArraySum_s * ArraySum_new(int *a, int *b)
{
    ArraySum_s *new_sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    new_sum->CalcSum =     Sum_implementation;
    new_sum->Destroy = Destroy_implementation;
    new_sum->nel = ArrayLength;
    for (int i=0; i<new_sum->nel; i++) {
        new_sum->in1[i] = a[i];
        new_sum->in2[i] = b[i];
    }
    return new_sum;
}

ArraySum_s *GetSum(int *array1, int *array2)
{
    ArraySum_s *sum = ArraySum_new(array1, array2);
    sum->CalcSum(sum);
    return sum;
}

void PrintSum(ArraySum_s *sum, char const *prefix)
{
    printf("%s:    [", prefix);
    int index_of_last = sum->nel - 1;
    for (int i=0; i<index_of_last; i++) { printf("%d, ", sum->out[i]); }
    printf("%d]\n", sum->out[index_of_last]);
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    ArraySum_s *sum1 = GetSum(array1, array2);
    PrintSum(sum1,  " first sum");

    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum2 = GetSum(array3, array4);
    PrintSum(sum2,  "second sum");

    ArraySum_s *total = GetSum(sum1->out, sum2->out);
    PrintSum(total, " total sum");

    sum1->Destroy(sum1);
    sum2->Destroy(sum2);
    total->Destroy(total);
}
