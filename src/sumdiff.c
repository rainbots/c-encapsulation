#include <stdio.h>

int Sum(int a, int b) { return a+b; }
int Diff(int a, int b) { return a-b; }

typedef struct
{
    int sum;
    int difference;
} Answer_s;

Answer_s SumDiff(int a, int b)
{
    Answer_s ans;
    ans.sum         = Sum(a,b);
    ans.difference  = Diff(a,b);
    return ans;
}

int main()
{
    Answer_s ans = SumDiff(2, 3);
    printf(
        "sum: %d, difference: %d\n",
        ans.sum, ans.difference
        );
}
