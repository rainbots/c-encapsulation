file-in-Vim-window: build/${file-stem}.exe
#file-in-Vim-window: build/${file-stem}.md

CFLAGS = -g -Wall -Wextra -pedantic

build/%.md: build/%.exe
	./$^ > $@

build/%.exe: src/%.c
	${compiler} $^ -o $@ ${CFLAGS}

clean-all-builds:
#	rm -f build/sumdiff.*
	rm -f build/sum-arrays.*
