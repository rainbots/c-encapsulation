# Table of Contents
- [Links to c-encapsulation docs](#markdown-header-links-to-c-encapsulation-docs)
- [C memory management models drive encapsulation](#markdown-header-c-memory-management-models-drive-encapsulation)
- [Examples](#markdown-header-examples)
    - [Do not return as an array](#markdown-header-do-not-return-as-an-array)
    - [Return as a struct](#markdown-header-return-as-a-struct)
    - [Wrong way to return an array](#markdown-header-wrong-way-to-return-an-array)
    - [Correct way but awkard way to return arrays](#markdown-header-correct-way-but-awkard-way-to-return-arrays)
    - [Return the array wrapped in a struct](#markdown-header-return-the-array-wrapped-in-a-struct)
    - [And this is why the heap is used](#markdown-header-and-this-is-why-the-heap-is-used)
    - [Return a pointer to the heap](#markdown-header-return-a-pointer-to-the-heap)
    - [Objects simplify using malloc and free](#markdown-header-objects-simplify-using-malloc-and-free)
    - [Final object for adding arrays](#markdown-header-final-object-for-adding-arrays)

---e-n-d---

# Links to c-encapsulation docs
Clone this repository:

    git clone https://rainbots@bitbucket.org/rainbots/c-encapsulation.git

Link to this repository:

    https://bitbucket.org/rainbots/c-encapsulation/src/master/

- [return an array][sumdiff-array] (example of what *not* to do)
[sumdiff-array]: https://bitbucket.org/rainbots/c-encapsulation/src/master/src/sumdiff.c

# C memory management models drive encapsulation
To a beginner, function calls seem like they can handle any refactoring task.
Encapsulate by writing functions, end-of-story. Example: if a calculation is
duplicated in code, the code is refactored to give the calculation a function
name and now the definition of that calculation occurs only once: the function
definition. Even for simple one-liners like given two numbers return the sum,
this is beneficial for extending and maintenance. This is encapsulation.

But this scheme quickly starts running into problems. The solutions tie back to
how memory management works. Understand the three models of memory management to
understand the solutions. Before stating the general problem as it relates to
memory management, explore a few examples and learn `gdb` to demonstrate with
each example that my mental model of memory management is in sync with how C
actually works.

---

Regardless of the language used to program a computer, memory is addressed in
bytes at the hardware level. Assembly directly deals with memory on a
byte-by-byte basis. If four bytes are working together to represent a single
value, an assembly programmer has to remember that the value is four bytes
large and make sure all four bytes are accessed every time the value is
accessed.

C makes assembly programming sane by providing datatypes. If a datatype is
larger than a byte, C handles accessing all of its bytes. For example, if an
array stores elements that are larger than a byte, C knows the datatype of the
array elements, and walking the array is the same `++` operator whether the
elements are one byte or multiple bytes.

C makes assembly programming sane by providing an assignment operator: *lhs* `=`
*rhs*. The behavior of `=` is very simple: copy the *rhs* to the *lhs*. The
*rhs* is a symbol for data. The physical meaning of the symbol is an address. In
`int a = 5`, `a` is the address of four bytes in memory that store the value
`5`. `a` on its own expands to `5`; `&a` expands to the address of the first
byte of the four bytes that store `5`. In `int b = 5`, `b` is again an address
of four bytes in memory. Since `b` and `a` are not the same symbol, this is a
different address even though `b` and `a` both store the same value. And even
though `b` and `a` store the same value, the value is in two memory locations
(`&a` and `&b`), so changing one value does not affect the other value. If one
value is deallocated, the other still exists. In `int c = a`, `c` is again an
alias for the numerical address of the four bytes, and the `=` operator copies
the value at `a` to this address. `c` and `a` are in no way linked, except that
at this line of code, they hold equal values.

C makes assembly programming sane by providing functions. C keeps track of the
function's address, the locals required by the function, and the function's
return datatype. C is strongly typed: the compiler checks that all function
calls are given the datatypes the function expects and all callers are returned
the datatype the caller expects. The function *expects* a type in the sense that
the function call is a sequence of instructions that include allocating memory
for the functions arguments. Because C is keeping track of how many bytes are
needed to define a single data value, the instructions for the function allocate
memory on the stack to hold that exact number of bytes. If a caller attempts to
pass more bytes, they would not fit in the allocated space. Too few bytes and
the value fits but is misintrepreted. So the compiler prevents those mistakes
when the programmer uses the abstraction.

Aside: C gives programmers the choice not to use that abstraction. Programmers
use the abstraction when the datatypes passed to the function are known at
compile time. If datatypes are unknown until runtime, the compiler cannot
determine how many bytes to allocate for that argument. The responsibility is
put back with the programmer to say how large the argument is. This is done with
*void pointers* to handle the general datatype, adding function arguments that
give the number of bytes that define the general data the pointer points to, and
function pointers to pass a datatype-specific implementation of whatever
function has to operate on that general data. At runtime the datatype is
known in the sense that the caller specifies the data it is sending to the
function, so it is also the caller's responibility to specify which function
implementation to use. An example of this is `qsort` in `stdlib.h`.

When a function returns a value, there must be a caller that has a line of code
with the *lhs* `=` *rhs* expression. The *rhs* is the function call. The *lhs*
is a datatype that matches the returned value. What can and cannot be returned
from a function is not a mystery. It follows the same rules as if the *rhs* was
not a function call. The function call expands to the return value. The only
difference is scope. Whatever value the function returns has local scope. If the
function adds two numbers, the sum is a local in that function's frame. The
address of the local is in the frame. When the function returns, the frame is
popped off the stack. After the `=`copies the value to the caller, the stack
memory that stored the frames locals is *deallocated* meaning it is freed-up
(garbage-collected) and considered available to overwrite.

This is why a function cannot return an array, but it can return an array
wrapped in a struct. The symbol that identifies the array does not hold the
entire array, only the address of the first element. It is like a `const`
pointer in that it cannot point to a different array. More than that, the array
size cannot be altered, and once initialized, the values can only be altered one
element at a time. So it is an ultra-restrictive version of `const` pointer.
This probably has to do with how C handles automatic memory allocation. In these
respects, an array is very different from a pointer. But as far as the value
stored at the symbol that identifies the array, it is the same as a pointer.
When the array is copied with `=` it is a shallow copy. Like a pointer, the
value being copied is the address of the first element. Now two symbols store
that address (the address is stored at two different addresses), but it is the
same address at both symbols, so they are both pointing at the same block of
memory. When the array is returned it is the same idea. Only that address is
returned. An important difference with functions is that the locals of a
function are allocated in the function's stack frame. As a memory location that
is allocated, the address of the array symbol and all other addresses in the
array hold locals to the function, so they are freed when the function returns.
The caller gets the correct address of the first element, but now it and the
address of all the other array elements are addresses where unpredictable
garbage resides.

The symbol for a struct behaves differently. It is not a pointer. Like an array,
it holds a continuous series of values in memory, but it is treated like a
non-pointer datatype. Non-pointer datatypes are treated as a single piece of
data, no matter how many bytes that datatype requires. A `char` is one byte and
its symbol is the address of that byte. An `int` is four bytes. Its symbol is
the address of the first byte. C provides an abstraction to make it easy to work
with all datatypes the same way, regardless of their size. *lhs* `=` *rhs*
doesn't only copy the first byte, it copies as many bytes as it has to to copy
the datatype. There are larger primitive data types that take more bytes, and
the `=` still hides the details that many bytes are being copied. The C struct
is a *custom* datatype but it is treated the same way. Its size is simply the
combined sum of the sizes of its members, plus any system-dependent padding.
Same as copying a primitive, the struct symbol is the address of the first byte
of its first member, but the `=` doesn't only copy the first byte, it copies all
the bytes required to represent that datatype. So when an array is wrapped in a
struct, the `=` doesn't just copy the array address. In fact, unlike how array
copying just aliases to the same memory location, the struct's array address is
not copied at all. The symbol on the *lhs* is a pnemonic for an address, but the
value stored at that address is the value stored at the address of the *rhs*.

---

# Examples
## Returning multiple values
First problem: given two numbers, return the sum and difference. First check a
simple `sum` program works:

=====[ c-encapsulation/sumdiff.c: just testing `Sum` ]=====
```c
#include <stdio.h>

int Sum(int a, int b) { return a+b; }
int main() { printf("sum: %d\n", Sum(2, 3)); }
```
Run it:
```bash
$ build/summdiff.exe 
sum: 5
```

Now try adding the `difference` part. This Python comma-separated return style
would be nice, but it is not valid C:

=====[ c-encapsulation/sumdiff.c: Python-style return is not valid C ]=====
```c
#include <stdio.h>

int Sum(int a, int b) { return a+b; }
int Diff(int a, int b) { return a-b; }

int, int SumDiff(int a, int b)  // This is not valid C!
{
    return Sum(a,b), Diff(a,b); // Neither is this!
}

int main()
{
    int sum, diff;
    sum, diff = SumDiff(2, 3);
    printf(
        "sum: %d, difference: %d\n",
        sum, diff
        );
}
```

## Do not return as an array

C only allows one data element to be returned. Try an array to hold the two
return values.

=====[ c-encapsulation/sumdiff.c: returning an array does not work ]=====
```c
#include <stdio.h>

int Sum(int a, int b) { return a+b; }
int Diff(int a, int b) { return a-b; }

int * SumDiff(int a, int b)
{
    int sumdiff[2];
    sumdiff[0] = a+b;
    sumdiff[1] = a-b;
    return sumdiff;
}

int main()
{
    int * sumdiff;
    sumdiff = SumDiff(2, 3);
    printf(
        "sum: %d, difference: %d\n",
        sumdiff[0], sumdiff[1]
        );
}
```

Running the program, suprisingly returning an array worked!
```bash
$ build/sumdiff.exe 
sum: 5, difference: -1
```

But there is no promise the values at `sumdiff[0]` and `sumdiff[1]` will still
be there if this were part of a larger program. This **is not** the right way to
return multiple values, even though it happens to work in this example.

C returns the array address, but the values at address and
address+1 were automatically allocated in stack memory for the function and are
deallocated when the function returns. The compiler warns this will happen:

```make
src/summdiff.c|11 col 12| warning: address of stack memory associated with local variable 'sumdiff' returned [-Wreturn-stack-address]
||     return sumdiff;
||            ^~~~~~~
|| 1 warning generated.
```

Since the program worked, using `gdb` to look at the array elements will not
shed any light. The frame for `SumDiff` is popped off the stack, but its locals
have not been overwritten yet, so doing a backtrace and `info locals` the array
elements still have the values. But try this in the debugger anyway:
```bash
$ gdb -q build/sumdiff.exe 
Reading symbols from build/sumdiff.exe...done.
(gdb) r
Starting program: /cygdrive/c/chromation-dropbox/Dropbox/c/TddF
ramework/TestDrive/kata/c-encapsulation/build/sumdiff.exe 
[New Thread 2648.0x1214]
[New Thread 2648.0x2fe4]
sum: 5, difference: -1
[Inferior 1 (process 2648) exited normally]
(gdb) b SumDiff
Breakpoint 1 at 0x10040112f: file src/sumdiff.c, line 9.
(gdb) r
Starting program: /cygdrive/c/chromation-dropbox/Dropbox/c/TddF
ramework/TestDrive/kata/c-encapsulation/build/sumdiff.exe 
[New Thread 10140.0x2e20]
[New Thread 10140.0x2884]

Thread 1 hit Breakpoint 1, SumDiff (a=2, b=3)
    at src/sumdiff.c:9
9           sumdiff[0] = a+b;
(gdb) bt
#0  SumDiff (a=2, b=3) at src/sumdiff.c:9
#1  0x0000000100401184 in main () at src/sumdiff.c:17
(gdb) info local
sumdiff = {-13344, 0}
(gdb) disp sumdiff
1: sumdiff = {-13344, 0}
(gdb) up
#1  0x0000000100401184 in main () at src/sumdiff.c:17
17          answer = SumDiff(2, 3);
(gdb) info local
answer = 0x0
(gdb) s
[New Thread 10140.0x1e90]
10          sumdiff[1] = a-b;
1: sumdiff = {5, 0}
(gdb) s
11          return sumdiff;
1: sumdiff = {5, -1}
(gdb) s
main () at src/sumdiff.c:20
20              answer[0], answer[1]
(gdb) info local
answer = 0xffffcb38
(gdb) disp answer
2: answer = (int *) 0xffffcb38
(gdb) disp answer[0]
3: answer[0] = 5
(gdb) disp answer[1]
4: answer[1] = -1
(gdb) s
18          printf(
2: answer = (int *) 0xffffcb38
3: answer[0] = 5
4: answer[1] = -1
(gdb) p &answer[0]
$1 = (int *) 0xffffcb38
```

This is interesting: even though the program prints the right answer, which
means `printf` is accessing `answer[0]` and `answer[1],` the debugger
*correctly* does not let me access the values at these addresses:
```bash
(gdb) p *answer[1]
Cannot access memory at address 0xffffffffffffffff
```

But it lets me access the address itself, because that is just pointer
arithmetic:
```bash
(gdb) p &answer[1]
$2 = (int *) 0xffffcb3c
```

---

## Return as a struct
Wrap the two return values in a struct.

=====[ c-encapsulation/sumdiff.c: Use a struct to return data ]=====
```c
#include <stdio.h>

int Sum(int a, int b) { return a+b; }
int Diff(int a, int b) { return a-b; }

typedef struct
{
    int sum;
    int difference;
} Answer_s;

Answer_s SumDiff(int a, int b)
{
    Answer_s ans;
    ans.sum         = Sum(a,b);
    ans.difference  = Diff(a,b);
    return ans;
}

int main()
{
    Answer_s ans = SumDiff(2, 3);
    printf(
        "sum: %d, difference: %d\n",
        ans.sum, ans.difference
        );
}
```

That works! C treats the two `int` members as a combined single value of an
`Answer_s` datatype. Trace the memory: the local `ans` in `SumDiff` is the
address of an `Answer_s` and the local `ans` in `main` is the address of a
different `Answer_s`.

Break at function `SumDiff` to look at the local `ans` before and after it is
assigned values from `Sum` and `Diff`.
```bash
$ gdb -q build/sumdiff.exe 
Reading symbols from build/sumdiff.exe...done.
(gdb) b SumDiff
Breakpoint 1 at 0x10040112c: file src/sumdiff.c, line 15.
(gdb) r
Starting program: /cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-enc
apsulation/build/sumdiff.exe 
[New Thread 13128.0x26ec]
[New Thread 13128.0x2454]

Thread 1 hit Breakpoint 1, SumDiff (a=2, b=3) at src/sumdiff.c:15
15          ans.sum         = Sum(a,b);
```

The struct in `SumDifff` begins with uninitialized data at address `0xffffcb30`.
```bash
(gdb) info local
ans = {sum = 1229148993, difference = 73}
(gdb) disp &ans
1: &ans = (Answer_s *) 0xffffcb30
(gdb) disp ans
2: ans = {sum = 1229148993, difference = 73}
```

Stepping through `SumDiff`, the struct members are assigned values:
```bash
(gdb) s
[New Thread 13128.0x2638]
Sum (a=2, b=3) at src/sumdiff.c:3
3       int Sum(int a, int b) { return a+b; }
(gdb) s
SumDiff (a=2, b=3) at src/sumdiff.c:16
16          ans.difference  = Diff(a,b);
1: &ans = (Answer_s *) 0xffffcb30
2: ans = {sum = 5, difference = 73}
(gdb) s
Diff (a=2, b=3) at src/sumdiff.c:4
4       int Diff(int a, int b) { return a-b; }
(gdb) s
SumDiff (a=2, b=3) at src/sumdiff.c:17
17          return ans;
1: &ans = (Answer_s *) 0xffffcb30
2: ans = {sum = 5, difference = -1}
```
The members of the local `ans` in `SumDiff` are assigned to the calculated values.

There are two function frames on the stack. I am in `SumDiff`, the *bottom* (#0)
of the stack.
```bash
(gdb) bt
#0  SumDiff (a=2, b=3) at src/sumdiff.c:17
#1  0x00000001004011a4 in main () at src/sumdiff.c:22
```

Go up the stack to look at the local `ans` in `main`.
```bash
(gdb) up
#1  0x00000001004011a4 in main () at src/sumdiff.c:22
22          Answer_s ans = SumDiff(2, 3);
```

Its members are still not assigned.
```bash
(gdb) info local
ans = {sum = 0, difference = 0}
```

And the `ans` in `main` is at a different stack memory address, `0xffffcb88`,
from the `0xffffcb30` `ans` in `SumDiff`:
```bash
(gdb) disp &ans
3: &ans = (Answer_s *) 0xffffcb88
(gdb) disp ans
4: ans = {sum = 0, difference = 0}
```

Comparing with the `ans` in `SumDiff` one more time:
```bash
(gdb) down
#0  SumDiff (a=2, b=3) at src/sumdiff.c:17
17          return ans;
(gdb) disp &ans
5: &ans = (Answer_s *) 0xffffcb30
(gdb) disp ans
6: ans = {sum = 5, difference = -1}
```

One more step and `SumDiff` is popped off the stack. Execution is back in
`main`. The `ans` in `main` now has the values copied from the returned `ans`.
```bash
(gdb) s
main () at src/sumdiff.c:25
25              ans.sum, ans.difference
3: &ans = (Answer_s *) 0xffffcb88
4: ans = {sum = 5, difference = -1}
(gdb) bt
#0  main () at src/sumdiff.c:25
(gdb) c
Continuing.
[New Thread 13128.0x2e58]
sum: 5, difference: -1
[Thread 13128.0x2e58 exited with code 0]
[Thread 13128.0x2638 exited with code 0]
[Inferior 1 (process 13128) exited normally]
(gdb) q
```

---

## Wrong way to return an array
This is the wrong way to add two arrays. The same problem as before: the
function tries to return an array. The addition happens in a for loop and its
generalizing the for loop for any size that makes this interesting.

```c
#include <stdio.h>

int * Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements
    int sum[nel];
    for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
    return sum;
}

#define ArrayLength 3
int main()
{
    int *sum;
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    sum = Sum(
        array1,
        array2,
        nel
        );
    printf(
        "[%d, %d, %d]",
        sum[0], sum[1], sum[2]
        );
}
```

The clang compiler warns against returning the address of a function local:
```make
/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-encapsulation/src/sum-arrays.c|7 col 12| warning: address of stack memory associated with local variable 'sum' returned [-Wreturn-stack-address]
||     return sum;
||            ^~~
|| 1 warning generated.
```

Surprisingly again, this actually works. But it will not work when the program
grows in size.

The g++ compiler gives the same warning about returning a local address, but
also says that variable length arrays are forbidden.
```make
|| src/sum-arrays.c: In function ‘int* Sum(int*, int*, int)’:
/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-encapsulation/src/sum-arrays.c|5 col 16| warning: ISO C++ forbids variable length array ‘sum’ [-Wvla]
||      int sum[nel];
||                 ^
/cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-encapsulation/src/sum-arrays.c|5 col 9| warning: address of local variable ‘sum’ returned [-Wreturn-local-addr]
||      int sum[nel];
||          ^~~
```

Switching the local `sum` in `Sum` to a pointer.
```c
#include <stdio.h>

int * Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements
    int * sum = NULL;
    for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
    return sum;
}
```

The compiler warned against an uninitialized pointer. It suggested pointing it
at NULL. Doing so results in a seg-fault:
```bash
$ ./build/sum-arrays.exe 
Segmentation fault (core dumped)
```

`sum` is initialized to NULL (0x0) and is never reassigned to a valid address.
When the for loop attempts to write to `*sum`, it cannot access memory at
address 0x0.
```bash
1: sum[0] = <error: Cannot access memory at address 0x0>
```

Here is the full session:
```bash
$ gdb -q build/sum-arrays.exe 
Reading symbols from build/sum-arrays.exe...done.
(gdb) s
The program is not being run.
(gdb) b Sum
Breakpoint 1 at 0x1004010f3: file src/sum-arrays.c, line 5.
(gdb) r
Starting program: /cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/
TestDrive/kata/c-encapsulation/build/sum-arrays.exe 
[New Thread 12076.0x1ea8]
[New Thread 12076.0x24b0]

Thread 1 hit Breakpoint 1, Sum (a=0xffffcb6c, b=0xffffcb60, nel=3)
    at src/sum-arrays.c:5
5           int * sum = NULL;
(gdb) info local
sum = 0xffffcb18
(gdb) info args
a = 0xffffcb6c
b = 0xffffcb60
nel = 3
(gdb) disp sum[0]
1: sum[0] = 4198866
(gdb) disp a[0]
2: a[0] = 6
(gdb) disp b[0]
3: b[0] = 7
(gdb) s
[New Thread 12076.0x17fc]
6           for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
1: sum[0] = <error: Cannot access memory at address 0x0>
2: a[0] = 6
3: b[0] = 7
(gdb) q
A debugging session is active.

        Inferior 1 [process 12076] will be killed.

Quit anyway? (y or n) n
Not confirmed.
(gdb) c
Continuing.

Thread 1 received signal SIGSEGV, Segmentation fault.
0x0000000100401136 in Sum (a=0xffffcb6c, b=0xffffcb60, nel=3)
    at src/sum-arrays.c:6
6           for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
1: sum[0] = <error: Cannot access memory at address 0x0>
2: a[0] = 6
3: b[0] = 7
(gdb) q
A debugging session is active.

        Inferior 1 [process 12076] will be killed.

Quit anyway? (y or n) n
Not confirmed.
(gdb) c
Continuing.
[New Thread 12076.0x3380]
      1 [main] sum-arrays 12076 cygwin_exception::open_stackdumpfile: Du
mping stack trace to sum-arrays.exe.stackdump
[Thread 12076.0x3380 exited with code 35584]
[Thread 12076.0x24b0 exited with code 35584]
[Inferior 1 (process 12076) exited with code 0105400]
(gdb) q
```

## Correct way but awkard way to return arrays
The solution is to allocate the memory in `main` and pass the pointer `sum` as
an argument to `Sum`.

```c
#include <stdio.h>

void Sum(int *a, int *b, int const nel, int *sum)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
}

#define ArrayLength 3
int main()
{
    int sum[ArrayLength];
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    Sum(array1, array2, nel, sum);
    printf(
        "[%d, %d, %d]",
        sum[0], sum[1], sum[2]
        );
}
```

It works but it is ugly: too many arguments in `Sum`.
Passing in an address of memory to hold the function's output hides the data
flow.

## Return the array wrapped in a struct
Here is the nicer solution using a struct:
```c
#include <stdio.h>

#define ArrayLength 3
typedef struct
{
    int ints[ArrayLength];
} IntList_s;

IntList_s Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements
    IntList_s sum;
    for (int i=0; i<nel; i++) { sum.ints[i] = a[i]+b[i]; }
    return sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    IntList_s sum = Sum(array1, array2, nel);
    printf(
        "[%d, %d, %d]",
        sum.ints[0], sum.ints[1], sum.ints[2]
        );
}
```

The only problem I see with this is performance as the array grows huge. It
would be nicer to make the array once and not have to copy it.

## And this is why the heap is used
There are **three** choices for memory management:

- **automatic** -- give the variable *function lifetime*\* (i.e., *local scope*):
    - the compiler [allocates and deallocates stack memory][stack-mem] to hold
      the local
    - the compiler places the allocation/deallocation at function entry/exit
      (this is the [call stack][call-stack])
    - \**function lifetime* is usually synonymous with *local scope*, but
      [with the `static` keyword][static-keyword] it is possible to have a local
      variable that is not automatic: it has *program lifetime* but is still
      namespaced *local* to the function
[stack-mem]: https://en.wikipedia.org/wiki/Stack-based_memory_allocation
[call-stack]: https://en.wikipedia.org/wiki/Call_stack#Functions_of_the_call_stack
[static-keyword]: https://en.wikipedia.org/wiki/Static_variable
- **static** -- give the variable *program lifetime*\* (i.e., *global scope*):
    - memory is allocated once when program execution begins and is not
      deallocated until the program terminates
    - uses [*static* memory][static-mem]:
        - only memory type with an absolute address
        - location is known by compiler at compile time
        - stored in the *data segment* if initialized, *BSS segment* if
          uninitialized
        - \**program lifetime* is usually synonymous with *global scope*, but
          again the special case is the use of `static` to declare a local
          variable
[static-mem]: https://en.wikipedia.org/wiki/Static_variable#Addressing
- **manual** -- lifetime is manually chosen by the programmer:
    - memory is allocated on the`heap` with `malloc`
    - memory is deallocated with `free`
    - also called [dynamic memory allocation][dynamic-mem]
    - the operating system handles identifying which address to use
    - the operating system handles defragmenting: as `malloc` and `free` are
      called, the heap is swiss-cheesed: between allocated blocks, there are
      freed blocks, and in a device with little RAM, the largest available
      continuous block shrinks as those freed blocks are not continuous.
      `avr-gcc` attempts to handle this, but [it is recommended][avr-ram] to
      keep the `heap` and `stack` separate by leaving the stack on internal RAM
      and moving to the `heap` to external RAM.
[dynamic-mem]: https://en.wikipedia.org/wiki/Memory_management#DYNAMIC
[avr-ram]: https://www.nongnu.org/avr-libc/user-manual/malloc.html

In the previous section, the question was how to write to a large block of data
from within a function and not have to copy it from the function local to the
caller's local. The example was adding two large arrays.

I cannot use stack memory:
    - If `main` allocates the local, the address has to be passed to the
      function as an argument and that is a crummy interface.
    - If `Sum` allocates the local, the sum is lost when the frame is
      popped.

I can use static memory to allocate memory for the array:
    - If the variable is global, it can be accessed from anywhere, but:
        - it is only associated with the function by naming convention, not by
          compiler-checkable enforcement
        - there is no compiler-protection against accidentally accessing the
          variable from another function
    - The solution to the above two problems is to put the global and the
      function that writes to it in its own file -- if the global is declared
      `static` it has internal linkage making it private to that file. This is
      self-defeating in this case because the whole point was to return the
      result to the caller without forcing the caller to allocate memory and
      make a copy. The variable scope is private to the `Sum` file. Creating an
      accessor in the `Sum` file does not help: that goes back to the caller
      allocating memory to hold the result. I suppose the accessor could be a
      pointer though. So maybe that's a way around this, I haven't tried. But
      even if that works, there are other problems.
    - Another problem is that I can only have one *instance* of taking a sum
      and storing the result. Imagine I want to add two arrays and store the
      result. Then add another two arrays and store the result. Then add those
      two results. This harks back to the earlier interface awkwardness: the
      caller has to allocate memory to hold the first result because calling the
      `Sum` function with new values overwrites the private array.

Speaking to that last problem with `static` memory, the desired behavior is that
the caller uses `Sum` as needed to add arrays, and each time `Sum` is invoked,
another block of memory is allocated to hold the result, and the caller has
access to those results without having to copy them, but at the same time the
results are namespaced to each `Sum` instance in a compiler-enforceable way.

Now the goal is clearly stated. The only way to achieve this behavior is to
manually allocate memory on the heap.

This is great, but be careful on small microcontrollers. Here is a high-level
view of the [issues around malloc and free in avr-gcc][avr-malloc].
[avr-malloc]: https://www.nongnu.org/avr-libc/user-manual/malloc.html

> The **standard RAM layout** is to place `.data` variables first, from the
> beginning of the *internal RAM*, followed by `.bss.` The stack is started from
> the top of *internal RAM*, growing downwards. The so-called "heap" available
> for the *dynamic memory allocator* will be placed beyond the end of `.bss.`
> Thus, **there's no risk that dynamic memory will ever collide with the *RAM*
> variables** (unless there were bugs in the implementation of the allocator).
> **There is still a risk that the heap and stack could collide** if there are
> large requirements for either dynamic memory or stack space. The former can
> even happen if the allocations aren't all that large but dynamic memory
> allocations get fragmented over time such that new requests don't quite fit
> into the "holes" of previously freed regions. Large stack space requirements
> can arise in a C function containing large and/or numerous local variables or
> when recursively calling a function.

TLDR: if not careful, the **heap can overrun** into the stack and viceversa.

## Return a pointer to the heap

First, here is an example using `malloc` syntax.
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

int * Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements

    int *sum = (int *)malloc(nel*sizeof(int));
    for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
    return sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    int *sum = Sum( array1, array2, nel);
    printf(
        "[%d, %d, %d]",
        sum[0], sum[1], sum[2]
        );
}
```

This works, but it is not a finished implementation of `Sum`. It relies on
program termination to free memory. Multiple calls to `Sum` will eventually
result in a memory leak.

But it is enough to verify how allocation on the `heap` works using `gdb`. Set
up breakpoints at `Sum`, `main:19`, and `main:20` to watch `Sum.sum` and
`main.sum` populat with values. Display the address stored in `sum` and the
three values accessible with `sum[]` notation.

```bash
$ gdb -q ./build/sum-arrays.exe 
Reading symbols from ./build/sum-arrays.exe...done.
(gdb) b Sum
Breakpoint 1 at 0x1004010f3: file src/sum-arrays.c, line 9.
(gdb) b 19
Breakpoint 2 at 0x1004011b9: file src/sum-arrays.c, line 19.
(gdb) r
Starting program: /cygdrive/c/chromation-dropbox/Dropbox/c/TddFramework/TestDrive/kata/c-encapsulation/build/s
um-arrays.exe 
[New Thread 15008.0x3978]
[New Thread 15008.0x3540]

Thread 1 hit Breakpoint 2, main () at src/sum-arrays.c:19
19          int *sum = Sum( array1, array2, nel);
(gdb) disp sum
1: sum = (int *) 0x80808000
(gdb) disp sum[0]
2: sum[0] = <error: Cannot access memory at address 0x80808000>
```

The `sum` local to `main` is not assigned to an accessible memory location yet.
I'd expected it to be a pointer to `NULL`, but it's not. It's some weird
`0x80808000` address. `gdb` reminds me I casted this as a pointer to an `int`.

Let `sum` get assigned to a valid memory address from `Sum` before accessing the
memory location it points to.
```bash
(gdb) c
Continuing.

Thread 1 hit Breakpoint 1, Sum (a=0xffffcb74, b=0xffffcb68, nel=3) at src/sum-arrays.c:9
9           int *sum = (int *)malloc(nel*sizeof(int));
(gdb) disp sum
3: sum = (int *) 0xffffcb18
(gdb) disp sum[0]
4: sum[0] = 4198866
(gdb) disp sum[1]
5: sum[1] = 1
(gdb) disp sum[2]
6: sum[2] = 1229148993
```

`Sum` local `sum` is assigned an address on the heap but I have not assigned
values to store there yet. Set a breakpoint just after the summing operation to
see that `Sum` local `sum` gets those values.
```bash
(gdb) b 11
Breakpoint 3 at 0x100401153: file src/sum-arrays.c, line 11.
(gdb) c
Continuing.
[New Thread 15008.0x37d8]

Thread 1 hit Breakpoint 3, Sum (a=0xffffcb74, b=0xffffcb68, nel=3) at src/sum-arrays.c:11
11          return sum;
3: sum = (int *) 0x60003b640
4: sum[0] = 13
5: sum[1] = 13
6: sum[2] = 13
(gdb) info b
Num     Type           Disp Enb Address            What
1       breakpoint     keep y   0x00000001004010f3 in Sum at src/sum-arrays.c:9
        breakpoint already hit 1 time
2       breakpoint     keep y   0x00000001004011b9 in main at src/sum-arrays.c:19
        breakpoint already hit 1 time
3       breakpoint     keep y   0x0000000100401153 in Sum at src/sum-arrays.c:11
        breakpoint already hit 1 time
```

Add one more breakpoint to do the same value check for the `sum` in `main`:
```bash
(gdb) b 20
Breakpoint 4 at 0x1004011ec: file src/sum-arrays.c, line 20.
(gdb) c
Continuing.

Thread 1 hit Breakpoint 4, main () at src/sum-arrays.c:20
20          printf(
1: sum = (int *) 0x60003b640
2: sum[0] = 13
(gdb) disp sum[1]
7: sum[1] = 13
(gdb) disp sum[2]
8: sum[2] = 13
(gdb) disp sum[3]
9: sum[3] = 1
```

As expected, the `sum` in `Sum` and the `sum` in `main` are assigned to the same
address: `0x60003b640`. The array was not copied, only a pointer to it. And
the caller did not have to allocate memory to store the sum, the function took
care of that.

## Objects simplify using malloc and free
Now the problem is how to free the memory. The function cannot do it. The caller
has to do it.

The program is identical but the last line is now a call to `free`:
```c
...
int main()
{
    ...
    free(sum);
}
```

Expand the program to calculate two sums and a total sum, creating a total of
three blocks of `sum` data on the `heap`:
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

int * Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements

    int *sum = (int *)malloc(nel*sizeof(int));
    for (int i=0; i<nel; i++) { sum[i] = a[i]+b[i]; }
    return sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    int *sum12 = Sum(array1, array2, nel);
    printf(
        "first sum:     [%d, %d, %d]\n",
        sum12[0], sum12[1], sum12[2]
        );
    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    int *sum34 = Sum(array3, array4, nel);
    printf(
        "second sum:    [%d, %d, %d]\n",
        sum34[0], sum34[1], sum34[2]
        );
    int *sum = Sum(sum12, sum34, nel);
    printf(
        "total sum:     [%d, %d, %d]\n",
        sum[0], sum[1], sum[2]
        );
    free(sum12); free(sum34); free(sum);
}
```

`Sum` is almost a class. It has a summing method that is called by `main`. That
same method is also a constructor. It constructs instances of sums. Refactor the
code turning `Sum` into a class. Abstract from `malloc` and `free`. Make the
constructing an explicit act of the caller and give the caller the ability to
destroy. Associate the create/destroy methods with the `Sum` class to make the
caller code readable.

The first step is to put the summed data into a struct and allocate those
structs on the heap instead of arrays. This is to illustrate changing to the
`->` syntax. Earlier I showed the `struct` is great for turning separate blocks
of data, like an array, into a single block of data: the two levels of
indirection of the array (a pointer) become a single level of indirection with a
struct (not a pointer). That struct was not passed around with pointers. This
one is. What's the deal?

I went down this path to:
- avoid copying large amounts of `sum` data (allocate it somewhere once and do all future work on that one memory location)
- to be able to work with multiple instances of `sum`s.

I showed the only way to achieve both goals is to use the `heap`. To use the
`heap`, I use `malloc` which returns a pointer to the memory on the `heap`. The
object goes on the heap. The object is a struct. The handle of the struct has to
be a pointer. There is no mechanism to allocate a struct on the heap without
using a pointer. Since I am not copying the data, this makes no difference. The
only difference is whether I use `.` or `->` to access members. For consistency
across libraries, Ben Klemens recommends standardizing on making all objects
pointers to structs.
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct
{
    int out[ArrayLength];
} ArraySum_s;

ArraySum_s * Sum(int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements

    ArraySum_s *sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    for (int i=0; i<nel; i++) { sum->out[i] = a[i]+b[i]; }
    return sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    ArraySum_s *sum12 = Sum(array1, array2, nel);
    printf(
        "first sum:     [%d, %d, %d]\n",
        sum12->out[0], sum12->out[1], sum12->out[2]
        );
    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum34 = Sum(array3, array4, nel);
    printf(
        "second sum:    [%d, %d, %d]\n",
        sum34->out[0], sum34->out[1], sum34->out[2]
        );
    ArraySum_s *sum = Sum(sum12->out, sum34->out, nel);
    printf(
        "total sum:     [%d, %d, %d]\n",
        sum->out[0], sum->out[1], sum->out[2]
        );
    free(sum12); free(sum34); free(sum);
}
```

Now make a *constructor* function `new`. Move the `malloc` from the `sum`
function (where it never really belonged) and put it in the `new`.

Make function `Sum` a member of the class using a function pointer.
Create a function pointer that matches the signature of `Sum`. Since `Sum` will
be part of the struct, the signature changes a little. Instead of returning a
pointer to an ArraySum_s, pass the struct to `Sum` and it stores the result by
acting on the struct.

This sounds like reverting back to the awkward interface idea where the caller
had to allocate memory and pass a pointer to that memory to the `Sum` function.
But I am not done refactoring yet. When I am done, the result will not be
awkward at all. It will be almost like standard object-oriented practice, except
that the lack of a [**pointer** to current instance][call-stack] means the
object is explicitly passed to its own methods. The caller is allocating memory,
but the details of the datatype are abstracted by `new`. And as this class
develops more functions, every one of them takes an ArraySum_s as its first
argument, regardless of which members the function actually uses.
[call-stack]: https://en.wikipedia.org/wiki/Call_stack#Functions_of_the_call_stack

Also make the definition of `Sum` a private implementation by declaring it
`static`. I renamed it because it makes the code more readable to identify it as
an `_implementation`, but it doesn't have to use a different name. The idea is
`Sum` goes in its own file `sum.c`, defining the `Sum` class. The definition of
the `Sum` function is only accessible to `sum.c` and is only used by `new`. Note
the `new` function cannot actually be called `new` because C++ already uses the
word `new` as a language construct that does exactly what my `new` is doing.
`g++` throws an error. So name the `new` function with the `lib` name followed
by `_new`.
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct ArraySum_s
{
    int out[ArrayLength];
    void (*Sum)(struct ArraySum_s *, int *, int *, int const);
} ArraySum_s;

static void Sum_implementation(ArraySum_s *sum, int *a, int *b, int const nel)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<nel; i++) { sum->out[i] = a[i]+b[i]; }
}

ArraySum_s * ArraySum_new(void)
{
    ArraySum_s *new_sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    new_sum->Sum = Sum_implementation;
    return new_sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    int const nel = ArrayLength;
    ArraySum_s *sum12 = ArraySum_new();
    sum12->Sum(sum12, array1, array2, nel);
    printf(
        "first sum:     [%d, %d, %d]\n",
        sum12->out[0], sum12->out[1], sum12->out[2]
        );
    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum34 = ArraySum_new();
    sum34->Sum(sum34, array3, array4, nel);
    printf(
        "second sum:    [%d, %d, %d]\n",
        sum34->out[0], sum34->out[1], sum34->out[2]
        );
    ArraySum_s *sum = ArraySum_new();
    sum->Sum(sum, sum12->out, sum34->out, nel);
    printf(
        "total sum:     [%d, %d, %d]\n",
        sum->out[0], sum->out[1], sum->out[2]
        );
    free(sum12); free(sum34); free(sum);
}
```

Now I pull the array inputs into the struct, at least superficially. I also
renamed `Sum` to `CalcSum` because without the input arguments the calls to
`Sum` are becoming less comprehensible.
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct ArraySum_s
{
    int nel;
    int *in1; int *in2;
    int out[ArrayLength];
    void (*CalcSum)(struct ArraySum_s *);
} ArraySum_s;

static void Sum_implementation(ArraySum_s *sum)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<sum->nel; i++) { sum->out[i] = sum->in1[i] + sum->in2[i]; }
}

ArraySum_s * ArraySum_new(int *a, int *b)
{
    ArraySum_s *new_sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    new_sum->CalcSum = Sum_implementation;
    new_sum->nel = ArrayLength;
    new_sum->in1 = a;
    new_sum->in2 = b;
    return new_sum;
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    ArraySum_s *sum12 = ArraySum_new(array1, array2);
    sum12->CalcSum(sum12);
    printf(
        "first sum:     [%d, %d, %d]\n",
        sum12->out[0], sum12->out[1], sum12->out[2]
        );
    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum34 = ArraySum_new(array3, array4);
    sum34->CalcSum(sum34);
    printf(
        "second sum:    [%d, %d, %d]\n",
        sum34->out[0], sum34->out[1], sum34->out[2]
        );
    ArraySum_s *sum = ArraySum_new(sum12->out, sum34->out);
    sum->CalcSum(sum);
    printf(
        "total sum:     [%d, %d, %d]\n",
        sum->out[0], sum->out[1], sum->out[2]
        );
    free(sum12); free(sum34); free(sum);
}
```

The above works, but I am sending a mixed message. The input arrays seem like
they are part of the object, but actually they are just pointers in the object.
The memory they point to is automatically allocated by `main`. Further refactor
by making methods to set the input values prior to calculating the `sum`.

I also forgot to put `Destroy` in the object before.
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct ArraySum_s
{
    int nel;
    int in1[ArrayLength]; int in2[ArrayLength];  // int *in1; int *in2;
    int out[ArrayLength];
    void (*CalcSum)(struct ArraySum_s *);
    void (*Destroy)(struct ArraySum_s *);
} ArraySum_s;

static void Sum_implementation(ArraySum_s *sum)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<sum->nel; i++) { sum->out[i] = sum->in1[i] + sum->in2[i]; }
}

static void Destroy_implementation(ArraySum_s *sum) { free(sum); }

ArraySum_s * ArraySum_new(int *a, int *b)
{
    ArraySum_s *new_sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    new_sum->CalcSum =     Sum_implementation;
    new_sum->Destroy = Destroy_implementation;
    new_sum->nel = ArrayLength;
    for (int i=0; i<new_sum->nel; i++) {
        new_sum->in1[i] = a[i];
        new_sum->in2[i] = b[i];
    }
    return new_sum;
}

int main()
{
    //=====[ Set up first sum ]=====
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    ArraySum_s *sum12 = ArraySum_new(array1, array2);
    sum12->CalcSum(sum12);
    printf(
        "first sum:     [%d, %d, %d]\n",
        sum12->out[0], sum12->out[1], sum12->out[2]
        );
    //=====[ Set up second sum ]=====
    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum34 = ArraySum_new(array3, array4);
    sum34->CalcSum(sum34);
    printf(
        "second sum:    [%d, %d, %d]\n",
        sum34->out[0], sum34->out[1], sum34->out[2]
        );
    //=====[ Get the total ]=====
    ArraySum_s *sum = ArraySum_new(sum12->out, sum34->out);
    sum->CalcSum(sum);
    printf(
        "total sum:     [%d, %d, %d]\n",
        sum->out[0], sum->out[1], sum->out[2]
        );
    sum12->Destroy(sum12);
    sum34->Destroy(sum34);
      sum->Destroy(sum);
}
```

It seems an odd thing to make the input arrays maintain their own copy in the
summing struct. And maybe this would not always be the right thing to do. But
now that the objects are completely decoupled from automatic memory management,
I can refactor main. The comments `Set up first sum` and `Set up second sum` are
begging to be functions. Use `main` just to print results.

## Final object for adding arrays
```c
#include <stdio.h>
#include <stdlib.h>

#define ArrayLength 3

typedef struct ArraySum_s
{
    int nel;
    int in1[ArrayLength]; int in2[ArrayLength];  // int *in1; int *in2;
    int out[ArrayLength];
    void (*CalcSum)(struct ArraySum_s *);
    void (*Destroy)(struct ArraySum_s *);
} ArraySum_s;

static void Sum_implementation(ArraySum_s *sum)
{   // add arrays a and b, nel is the number of elements
    for (int i=0; i<sum->nel; i++) { sum->out[i] = sum->in1[i] + sum->in2[i]; }
}

static void Destroy_implementation(ArraySum_s *sum) { free(sum); }

ArraySum_s * ArraySum_new(int *a, int *b)
{
    ArraySum_s *new_sum = (ArraySum_s *)malloc(sizeof(ArraySum_s));
    new_sum->CalcSum =     Sum_implementation;
    new_sum->Destroy = Destroy_implementation;
    new_sum->nel = ArrayLength;
    for (int i=0; i<new_sum->nel; i++) {
        new_sum->in1[i] = a[i];
        new_sum->in2[i] = b[i];
    }
    return new_sum;
}

ArraySum_s *GetSum(int *array1, int *array2)
{
    ArraySum_s *sum = ArraySum_new(array1, array2);
    sum->CalcSum(sum);
    return sum;
}

void PrintSum(ArraySum_s *sum, char const *prefix)
{
    printf("%s:    [", prefix);
    int index_of_last = sum->nel - 1;
    for (int i=0; i<index_of_last; i++) { printf("%d, ", sum->out[i]); }
    printf("%d]\n", sum->out[index_of_last]);
}

int main()
{
    int array1[ArrayLength] = {6, 7, 6};
    int array2[ArrayLength] = {7, 6, 7};
    ArraySum_s *sum1 = GetSum(array1, array2);
    PrintSum(sum1, "first sum");

    int array3[ArrayLength] = {-3, -4, -3};
    int array4[ArrayLength] = {-4, -3, -4};
    ArraySum_s *sum2 = GetSum(array3, array4);
    PrintSum(sum2, "second sum");

    ArraySum_s *total = GetSum(sum1->out, sum2->out);
    PrintSum(total, "total sum");

    sum1->Destroy(sum1);
    sum2->Destroy(sum2);
    total->Destroy(total);
}
```
Output:
```bash
$ ./build/sum-arrays.exe
 first sum:    [13, 13, 13]
second sum:    [-7, -7, -7]
 total sum:    [6, 6, 6]
```

I cannot think of any more ways to refactor either the `array_sum` class or
`main`. I think this shows everything I need to know right now about making
objects.

I tried to put compound literals as inputs to `GetSum` instead of declaring
arrays first, just to make the code more readable, but g++ throws an error about
taking the address of a temporary array. I decided that was a reasonable point.
Even though clang is fine with it. It's not a big sacrifice in readability.

## Abstract data types are a better default object template
I did all of the above *because I could*, *not because I should*. It is possible
to slavishly imitate the appearance of OO code from other languages using the
`instance->member` notation, but when writing the `mock-c` module, I found this
actually harmed encapsulation. The real benefit of OO thinking is hiding
implementation to promote modularity and reduce cognitive load on the client.
Modularity, i.e., the making of truly distinct modules, promotes maintainability
and composability. And I found in `mock-c` that this ended up making the code
more readable than the `instance->member` version.

Here is [a great talk by Kevlin Henney][kevlin-c].
[kevlin-c]: https://vimeo.com/131640723

### My muddy thoughts before hearing Kevlin Henney explain abstract data types
- The stub definitions need to access the lists.
- I can make the list globals and pull them out of the object.
- Or I can make the object global.

Either way, I need to make the mock object global, at least in the client test
file, so that `setUp` and `tearDown` can access it.

That makes me realize that C objects (i.e., the pointer to an instance of the
object) do need global scope, at least within a single file. This is the only
way for functions to access data in the same instance without taking the object
as an input parameter. But globals are singletons... And singletons are bad...

Is a global mock object a singleton? Not necessarily. A singleton has some
mechanism to enforce that only one instance exists. There is nothing stopping me
from making more instances just because the object is global. If I made it
global in the mock-c.c file, and declared it extern in the mock-c.h file to give
the client test code access to it, that would be a little different. The client
test code would not need to define the pointer to the mock object, it could just
pass `mock` to its mock-c API.

My default was to leave a global mock pointer to a `Mock_s` and to place the
`Mock_s` full definition in the `mock-c.h` file, giving the client code direct
access to its members. Then I went through the following example and realized I
should hide the definition.

### Kevlin Henney abstract datatype example
The struct is forward declared in the header. The struct definition is in the
.c. This simple prescription has interesting consequences: the resulting C
object provides encapsulation, the code is more readable. Using this technique
to revise mock-c where I was both the author of the module and the client code,
the compiler enforced the encapsulation: any attempts to directly access struct
members from the client code results in an error because that implementation is
invisible.

=====[ RecentlyUsedList.h ]=====
```c
typedef struct RecentlyUsedList RecentlyUsedList;

RecentlyUsedList *create();
void destroy(RecentlyUsedList *);
bool isEmpty(RecentlyUsedList const *);
size_t size(RecentlyUsedList const *);
void add(RecentlyUsedList *, int toAdd);
int get(RecentlyUsedList const *, int index);
bool equals(RecentlyUsedList const *, RecentlyUsedList const *);
```

=====[ RecentlyUsedList.c ]=====
```c
struct RecentlyUsedList
{
    size_t length;
    int *items;
};

RecentlyUsedList *create()
{
    RecentlyUsedList *result = (RecentlyUsedList *)malloc(sizeof(RecentlyUsedList));
    result->length = 0;
    result->items = 0;
    return result;
}
void destroy(RecentlyUsedList *self)
{
    free(self->items);
    free(self);
}
bool isEmpty(RecentlyUsedList const *self)
{
    return (self->length==0);
}
size_t size(RecentlyUsedList const *self)
{
    return self->length;
}
static int indexOf(RecentlyUsedList const *self, int toFind)
{
    int result = -1;
    for (int index=0; result != -1 && index != self->length; ++index)
        if (self->items[index] == toFind)
            result = index;
    return result;
}
static void removeAt(RecentlyUsedList const *self, int index)
{
    memmove(
        &self->items[index],
        &self->items[index + 1],
        (self->length - index - 1) * sizeof(int)
        );
    --self->length;
}
void add(RecentlyUsedList *self, int toAdd)
{
    int found = indexOf(self, toAdd);
    if(found != -1)
        removeAt(self, found);
    self->items = (int *)realloc(self->items, (self->length + 1) * sizeof(int));
    self->items[self->length] = toAdd;
    ++self->length;
}
int get(RecentlyUsedList const *self, int index)
{
    return self->items[index];
}
bool equals(RecentlyUsedList const *lhs, RecentlyUsedList const *rhs)
{
    return lhs->length == rhs->length && memcmp(
        lhs->items,
        rhs->items,
        lhs->length * sizeof(int)
        ) == 0;
}
```

#### What is forward declaration?
I've been doing this everytime I define a struct that is an object, I just
didn't realize it. Forward declaration is to anounce the presence of a struct
before it is defined. This is to allow declaring the struct datatype as an input
parameter or a return value in places in the code where it is inconvenient or
impossible to put the definition before the first use. Same idea as function
prototypes.

An abstract data type is a struct that is forward declared in the .h and defined
in the .c. This is the same idea as a public function: the prototype is in the
.h, but the implementation is hidden in the .c. Making the analogy to a
type definition, a name is all that is needed for a datatype prototype. The
implementation *is* the members it contains.

#### Why forward declare?
This is what makes it an *abstract* data type.

#### Implications for objects in C:
Client code cannot access struct members directly. The compiler does not allow
it because the client code does not know the struct definition at compile time.
This is a wonderful feature to enforce a good default seam in how classes are
defined.

My initial temptation when I decided "I am going to make an object in C" was to
imitate the instance.member or instance->member notation that *real*
object-oriented langauges use. So I put every function into the struct. But this
requires making the full struct definition visible to the client code. That
needlessly throws away encapsulation.

Instead, if the full struct definition is hidden, the client cannot access
members with the classic OOP notation. The implementation of the class methods
uses the OOP notation in full force, but the client code only accesses instance
members via functions. The API is functions only. I'm sure there are times the
classic-object notation is appropriate, but as the default, the abstract data
type seems the way to go.


